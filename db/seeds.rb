# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

all_artist = Artist.create([
	{name: 'Adele', description: 'Adele Laurie Blue Adkins, conocida simplemente como Adele, es una cantante, compositora y multinstrumentista británica.'},
	{name: 'Bob Marley', description: 'Robert Nesta Marley, más conocido como Bob Marley, fue un músico, guitarrista y compositor jamaicano. Durante su carrera musical fue el líder, compositor y guitarrista de las bandas de ska, rocksteady y reggae The Wailers y Bob Marley & The Wailers.'},
	{name: 'Bruno Mars', description: 'Peter Gene Hernández, conocido como Bruno Mars, es un cantante, compositor, productor musical y coreógrafo estadounidense. Mars comenzó a crear música desde una edad muy temprana y actuó en muchos escenarios de su pueblo natal a lo largo de su niñez realizando imitaciones.'},
	{name: 'Ed Sheeran', description: 'Edward Christopher Sheeran, más conocido como Ed Sheeran o Ed es un cantante, compositor y actor guitarrista británico.​ A corta edad, comenzó a cantar en la iglesia a la que asistía y también aprendió a tocar la guitarra.'},
	{name: 'Rawayana', description: 'Rawayana es una banda venezolana de reggae y trippy pop formada en 2007 en Caracas. Está integrada por Alberto «Beto» Montenegro, Antonio «Tony» Casas, Andrés Story y Alejandro Abeijón. Es reconocida por su mezcla de reggae y ska. '}
])

Lp.create(name: '19', description: '19 is the debut studio album by English singer-songwriter Adele. It was first released on 28 January 2008, through XL Recordings.', artist: all_artist.first)
Lp.create(name: '21', description: '21 is the second studio album by English singer-songwriter Adele. It was released on 24 January 2011 in Europe and on 22 February 2011 in North America by XL Recordings and Columbia Records. The album was named after the age of the singer during its production.', artist: all_artist.first)
Lp.create(name: '25', description: '25 is the third studio album by English singer Adele, released on 20 November 2015 by XL Recordings and Columbia Records.', artist: all_artist.first)

Lp.create(name: 'Legend', description: 'Legend is a compilation album by Bob Marley and the Wailers. It was released in May 1984 by Island Records.', artist: all_artist.second)

Lp.create(name: 'Doo Wops & Hooligans', description: 'Doo-Wops & Hooligans is the debut studio album by American singer-songwriter Bruno Mars, which was released on October 4, 2010 by Atlantic and Elektra Records.', artist: all_artist.third)
Lp.create(name: 'Unorthodox Jukebox', description: 'Unorthodox Jukebox is the second studio album by American singer and songwriter Bruno Mars. It was released on December 7, 2012, by Atlantic Records. It serves as the follow-up to Mars debut record Doo-Wops & Hooligans.', artist: all_artist.third)
Lp.create(name: '24K Magic', description: '24K Magic is the third studio album by American singer and songwriter Bruno Mars. It was released worldwide on November 18, 2016, by Atlantic Records.', artist: all_artist.third)

Lp.create(name: 'X', description: 'x is the second studio album by English singer-songwriter, Ed Sheeran. It was released on 20 June 2014 in Australia and New Zealand, and worldwide on 23 June through Asylum Records and Atlantic Records. The album received positive reviews from music critics.', artist: all_artist.fourth)
Lp.create(name: 'Divide', description: 'Is the third studio album by English singer-songwriter Ed Sheeran. It was released on 3 March 2017 through Asylum Records and Atlantic Records. "Castle on the Hill" and "Shape of You" were released as the album lead singles on 6 January 2017.', artist: all_artist.fourth)

Lp.create(name: 'Rawayanaland', description: 'Rawayanaland is the second album of the Venezuelan band of reggae Rawayana released on 2013.', artist: all_artist.last)
Lp.create(name: 'Trippy Caribbean', description: 'After six years of carrer', artist: all_artist.last)