class CreateLps < ActiveRecord::Migration[5.1]
  def change
    create_table :lps do |t|
      	t.references :artist, foreign_key: true
	 	t.string :name
	 	t.text :description
	   	t.string :image_file_name
	   	t.string :image_content_type
	  	t.string :image_file_size
	   	t.datetime :image_updated_at
      t.timestamps
    end
  end
end