class LpsController < ApplicationController
	def index
    @lps = Lp.all
  end

	def show
		@lp = Lp.find(params[:id])
	end

	def new
		@lp = Lp.new
  end

  def edit
		@lp = Lp.find(params[:id])
	end

	def create
		@lp = Lp.new(lp_params)

		if @lp.save
  		redirect_to @lp
  	else
  		render 'new'
  	end
	end

	def update
		@lp = Lp.find(params[:id])
	 
	  	if @lp.update(lp_params)
	    	redirect_to @lp
	  	else
	    	render 'edit'
	  	end
	end

	def destroy
	  @lp = Lp.find(params[:id])
	  @lp.destroy
	 
	  redirect_to @lp
	end

	private
	def lp_params
  	params.require(:lp).permit(:artist_id, :name, :description, :image)
	end
end
