class Lp < ApplicationRecord
 	belongs_to :artist
  	has_attached_file :image
	validates_attachment_file_name :image, :matches => [/png\Z/, /jpeg\Z/, /gif\Z/, /jpg\Z/]
	validates :name, presence: true
	validates :artist_id, presence: true
end
