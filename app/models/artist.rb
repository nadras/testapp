class Artist < ApplicationRecord
	has_many :lps, dependent: :destroy
	has_attached_file :image
	validates_attachment_file_name :image, :matches => [/png\Z/, /jpeg\Z/, /gif\Z/, /jpg\Z/]
	validates :name, presence: true
	validates :name, uniqueness: true
end
